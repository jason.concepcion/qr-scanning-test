import { NextPage } from 'next';
import { useEffect } from 'react';

const AppLinks: NextPage = (): React.ReactElement => {
  useEffect(() => {
    const UA = navigator.userAgent;
    const isMobile = /iPhone|iPad|iPod|Android/i.test(UA);
    if (isMobile) {
      alert(UA);
    }
    // alert(isMobile);
  }, []);

  return (
    <div>
      <code> hello world !</code>
    </div>
  );
};

export default AppLinks;
